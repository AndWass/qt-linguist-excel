TEMPLATE = app
CONFIG += qt console c++11
QT += core xml

include(3rdparty/QtXlsxWriter/src/xlsx/qtxlsx.pri)

SOURCES += \
    main.cpp
