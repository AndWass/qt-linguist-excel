#include "xlsxdocument.h"
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QStringList>
#include <QXmlStreamAttributes>
#include <QXmlStreamReader>
#include <map>

struct translation
{
    bool finished = false;
    QString text;
};

struct message
{
    QString filename;
    QString line;
    QString source;
    translation trans;
};

struct context
{
    QString name;
    std::vector<message> messages;
};

context read_xml_context(QXmlStreamReader &rdr)
{
    context retval;
    while (!rdr.atEnd())
    {
        rdr.readNext();
        if (rdr.isEndElement() && rdr.name() == "context")
        {
            return retval;
        }

        if (rdr.isStartElement() && rdr.name() == "name")
        {
            if (rdr.readNext() && rdr.isCharacters())
            {
                retval.name = rdr.text().toString();
                rdr.readNext();
            }
        }
        else if (rdr.isStartElement() && rdr.name() == "message")
        {
            bool is_end_message;
            message msg;
            do
            {
                rdr.readNext();
                if (rdr.isStartElement() && rdr.name() == "location")
                {
                    auto attr = rdr.attributes();
                    msg.filename = attr.value("filename").toString();
                    msg.line = attr.value("line").toString();
                }
                else if (rdr.isStartElement() && rdr.name() == "source")
                {
                    while (rdr.readNext() != QXmlStreamReader::Characters)
                    {
                    }
                    msg.source = rdr.text().toString();
                }
                else if (rdr.isStartElement() && rdr.name() == "translation")
                {
                    auto attr = rdr.attributes();
                    if (attr.hasAttribute("type") &&
                        attr.value("type") == "unfinished")
                    {
                        msg.trans.finished = false;
                    }
                    if (rdr.readNext() == QXmlStreamReader::Characters)
                    {
                        msg.trans.text = rdr.text().toString();
                    }
                }
                is_end_message = rdr.isEndElement() && rdr.name() == "message";
            } while (!is_end_message);
            retval.messages.push_back(msg);
        }
    }

    return retval;
}

void generate_xlsx(QString source, QString dest)
{
    std::map<QString, std::vector<message>> contexts;

    QFile fio(source);
    if (fio.open(QFile::ReadOnly))
    {
        QXmlStreamReader rdr(&fio);

        while (!rdr.atEnd())
        {
            rdr.readNext();
            if (rdr.isStartElement() && rdr.name() == "context")
            {
                auto ctx = read_xml_context(rdr);
                auto &existing_context = contexts[ctx.name];
                existing_context.insert(existing_context.begin(),
                                        ctx.messages.begin(),
                                        ctx.messages.end());
            }
        }
    }

    QXlsx::Document xlsx;
    int row = 1;
    int col = 1;
    xlsx.write(row, col++, "Context name");
    xlsx.write(row, col++, "File");
    xlsx.write(row, col++, "Line");
    xlsx.write(row, col++, "Source");
    xlsx.write(row, col++, "Translation");
    row++;
    for (auto iter = contexts.begin(); iter != contexts.end(); iter++)
    {
        QString context_name = iter->first;
        for (auto &m : iter->second)
        {
            col = 1;
            xlsx.write(row, col++, context_name);
            xlsx.write(row, col++, m.filename);
            xlsx.write(row, col++, m.line);
            xlsx.write(row, col++, m.source);
            xlsx.write(row, col++, m.trans.text);
            row++;
        }
    }

    xlsx.saveAs(dest);
}

void generate_ts(QString source, QString dest, QString lang)
{
    std::map<QString, std::vector<message>> contexts;
    QXlsx::Document xlsx(source);
    for (int i = 2; i < 1000; i++)
    {
        auto context_name = xlsx.read(i, 1).toString();
        message msg;
        msg.filename = xlsx.read(i, 2).toString();
        msg.line = xlsx.read(i, 3).toString();
        msg.source = xlsx.read(i, 4).toString();
        msg.trans.text = xlsx.read(i, 5).toString();

        if (msg.source == "")
        {
            break;
        }
        msg.trans.finished = msg.trans.text.length() > 0;

        contexts[context_name].push_back(msg);
    }

    QFile file(dest);
    if (file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QXmlStreamWriter writer(&file);
        writer.writeStartDocument();
        writer.writeCharacters("\n");
        writer.writeDTD("<!DOCTYPE TS>");
        writer.writeCharacters("\n");
        writer.writeStartElement("TS");
        writer.writeAttribute("version", "2.0");
        writer.writeAttribute("language", lang);
        writer.writeCharacters("\n");

        for (auto iter = contexts.begin(); iter != contexts.end(); iter++)
        {
            writer.writeStartElement("context");
            writer.writeCharacters("\n    ");

            writer.writeStartElement("name");
            writer.writeCharacters(iter->first);
            writer.writeEndElement();
            writer.writeCharacters("\n");

            for (message &m : iter->second)
            {
                writer.writeCharacters("    ");
                writer.writeStartElement("message");
                writer.writeCharacters("\n");

                writer.writeCharacters("        ");
                writer.writeEmptyElement("location");
                writer.writeAttribute("filename", m.filename);
                writer.writeAttribute("line", m.line);
                writer.writeCharacters("\n");

                writer.writeCharacters("        ");
                writer.writeStartElement("source");
                writer.writeCharacters(m.source);
                writer.writeEndElement();
                writer.writeCharacters("\n");

                writer.writeCharacters("        ");
                writer.writeStartElement("translation");
                if (m.trans.finished)
                {
                    writer.writeCharacters(m.trans.text);
                }
                else
                {
                    writer.writeAttribute("type", "unfinished");
                }
                writer.writeEndElement();
                writer.writeCharacters("\n");

                writer.writeCharacters("    ");
                writer.writeEndElement();
                writer.writeCharacters("\n");
            }

            writer.writeEndElement();
            writer.writeCharacters("\n");
        }

        writer.writeEndElement();
        writer.writeEndDocument();
    }
    else
    {
        qDebug() << "Failed to open file: " << file.errorString();
    }
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("Qt Linguist Excel converter");
    QCoreApplication::setApplicationVersion("0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription("Linguist to Excel converter");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption languageOption(QStringList() << "l"
                                                    << "lang",
                                      "Language tag to use", "language");
    parser.addOption(languageOption);
    parser.addPositionalArgument(
        "source", QCoreApplication::translate(
                      "main", "Source file, either a ts or xlsx file"));
    parser.addPositionalArgument(
        "destination",
        QCoreApplication::translate(
            "main", "Destination file, either a ts or xlsx file"));

    parser.process(app);
    auto source_dest = parser.positionalArguments();
    if (source_dest.size() < 2)
    {
        qDebug() << "Must supply both source and destination" << source_dest;
        return 0;
    }

    auto iter = source_dest.rbegin();
    QString dest = *iter++;
    QString source = *iter++;
    if (source.endsWith(".ts") && dest.endsWith(".xlsx"))
    {
        generate_xlsx(source, dest);
    }
    else if (source.endsWith(".xlsx") && dest.endsWith(".ts"))
    {
        QString lang = "";
        if (parser.isSet(languageOption))
        {
            lang = parser.value(languageOption);
        }
        generate_ts(source, dest, lang);
    }
    else
    {
        qDebug() << "Bad source and destination file combination";
    }
}
